# Assignment-4

Creating a Computer Store with JS, HTML and a bit of CSS. 

## Name
Computer Store.

## Description
This project is for learning and practicing purposes only.

## Installation
Make sure you have installed at least the following tools:
• A text editor of your choice (Visual Studio Code recommended)
• Live Server VS add-on
You will also use your Browser’s Developer Tools for testing and debugging.

## Usage
Learning purposes

## Support
Contact the author.

## Contributing
Open to contributions
Requirements:
Use the standard Gitlab rules before pushing your code!
```
cd existing_repo
git remote add origin https://gitlab.com/Petar-Programing-Petrov/assignment-4.git
git branch -M main
git push -uf origin main
```

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Author : Petar Petrov Email: petar.dragov.petrov@gmail.com


## Project status
Development has slowed down because of some issues with getting use to the For-each, for-in and all other for cycles in JS, also had a  bit trouble getting the return value from a selected element in drop down menu. And the style is not the best one but its clean. HTML and CSS were also a bit challenging. But at least the logic behind the scenes works fine.   

