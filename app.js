//Elements to map in the HTML file

let loan = 0;
let earnings = 0;
let constrains = 0;
let bankBalance = 0;
let laptops = [];
let  laptopsPrice = 0;
const laptopsElements = document.getElementById("laptopsId");
let laptopsPriceElement = document.getElementById("laptopsPriceId").innerHTML = ((new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(laptopsPrice)));
const buyNowElement = document.getElementById("buyNowDisplyText");
let laptopsFeatures = document.getElementById("features");
let bankBalanceElement = document.getElementById("balance").innerHTML = ((new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(bankBalance)));
let workElement = document.getElementById("payAmount").innerHTML = ((new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(earnings)));

//Fetching the data from URL, importing the data to the array and calling addLaptopsToMenu so we can also add the laptops to dropdown menu

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToMenu(laptops));

//iterating trough the array and importing the laptops  (laptops Array we got in fetch)

const addLaptopsToMenu = function (laptops) {

  laptops.forEach(lap => addLaptopToMenu(lap));

}

//Adding laptop's title to the drop down menu

const addLaptopToMenu = function (laptop) {

  const laptopElement = document.createElement("option", value="dropMenuValue"); // Creating options in the dropdown in the HTML
  laptopElement.value = laptop.title;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElements.appendChild(laptopElement);
  
}

//Adding and displaying the information for the laptops. Price, Features, Description, etc...
const getSelectedValue = laptop => {
  const selectedLaptop = laptops[laptop.target.selectedIndex -1 ];
  laptopsFeatures.innerText = selectedLaptop.specs;
  buyNowElement.innerText  = selectedLaptop.description;
  laptopsPriceElement.innerText = selectedLaptop.price;
  laptopsPrice = selectedLaptop.price;
  laptopsPriceElement = document.getElementById("laptopsPriceId").innerHTML = ((new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(laptopsPrice)));
   
}

//After choosing from  the dropdown menu we trigger the function to getSelectedValue so we can start searching in the array the needed information to show.
laptopsElements.addEventListener("change", getSelectedValue);
 
//adding function to the Get A Loan Button.
//On clicking the get a loan button we call the function.
function getALoanButton(){
  let getALoanAmount = window.prompt("Please enter the amount you need: ");
  let loanAmount = parseInt(getALoanAmount);
  
  if(loan > 0){

    alert(`You have to return the existing loan of ${loan} first`);
    
  }
  if (loanAmount  >= bankBalance * 2) {

    alert(`You cannot get a loan more than double of your bank balance`);

  }
  if(loanAmount > 0 && loan === 0 && loanAmount < bankBalance * 2 ){
    loan += loanAmount;
    bankBalance += loan;
    alert(`You got a loan from ${loanAmount}`);
    bankBalanceFunction();
    //bankBalanceElement = document.getElementById("balance").innerHTML = ((new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(bankBalance)));
  }

}
//After clicking the return loan we will return the loan we have got.
function returnALoanButton(){
  if (bankBalance >= loan) {    
    bankBalance -= loan;
    loan = 0;    
    bankBalanceFunction();
  }
  if(loan > bankBalance){
    alert(`You have returned: ${bankBalance} and have to return: ${loan - bankBalance} more!`)
    bankBalance -=loan -bankBalance;
    bankBalanceFunction();
  }
  else if (loan <= 0){
    
    alert(`Your actual loan is: ${loan}`)
  }
}
//Adding what happens after pressing the work button. 
function workButton(){

  earnings += 100;
  workFunction();
}

//After clicking the button we transfer earnings to our bank.
function transferToBank(){
  if (loan > 0) { //if we have a loan then we deduce 10% of the salary before transferring to the bank
    constrains +=  earnings * 10 /100; //we get the earnings after returning 10% for the loan
    earnings -= constrains; //Real earnings after returning 10% for the loan 
    loan -= constrains; // removing the returned 10% from loan so we have to return less after
    bankBalance += earnings; //Finlay we move the earnings in our bank
    earnings = 0; //Resetting the earnings after we transferred them to the bank account
    bankBalanceFunction();
    workFunction();
  }
  else{ //If we dont have to return the loan 
    bankBalance += earnings; //We just add the earnings from work to our bank account
    earnings = 0; //resetting the earnings after we transferred them to bank account 
    bankBalanceFunction();
    workFunction();
  }
  
}

//Adding functionality to the buy now button.
function buyNowButton(){

  

  if (bankBalance == 0) {
    alert("You have no money but you can go to Work or get a Loan")
  }
  else if(bankBalance > laptopsPrice){
    bankBalance -= laptopsPrice;
    alert("You have bought the Laptop you want!")
    bankBalanceFunction();
  }
  else{

    alert("You dont have enough money to buy this laptop")
  }
  
}
//Created the function to update the bank content.
function bankBalanceFunction(){
  bankBalanceElement = document.getElementById("balance").innerHTML = ((new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(bankBalance)));
}
//Created this function to add money on every click.
function workFunction(){
  workElement = document.getElementById("payAmount").innerHTML = ((new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(earnings)));
}





